#print out type of each data type in the console
print(type("0, 1, 2, -3, 128, 0b111, 0x7E3"))
print(type("0,123, 256.2, -3.14, 1e10"))
print(type(1 - 2j), (30 + 15j))
print(type("house"), (""), ("This is a sentence"), ('simple quotes'))
print (type(5 > 4), (2 > 4))
#print(type())